class Paciente:
    acesso = 'Restrito'

    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        self.atendido = False
        self.atendido_por = ''

    def ser_atendido(self, doutor):
        self.atendido = True
        self.atendido_por = doutor

    @classmethod
    def para_retorno(cls, doutor, nome, idade):
        novo_paciente = cls(nome, idade)
        novo_paciente.ser_atendido = doutor.nome

        return novo_paciente

class Doutor:
    acesso = 'Controlado'

    def __init__(self, nome):
        self.nome = nome

class Classificador:

    @staticmethod
    def classificar_idade(paciente: Paciente):
        if paciente.idade < 12:
            return 'Criança'
        if paciente.idade < 18:
            return 'Adolescente'
        if paciente.idade < 60:
            return 'Adulto'
        return 'Idoso'
